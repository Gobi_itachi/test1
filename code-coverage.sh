#!/bin/bash

# TODO: deal with no previous pipeline / null coverage value

# get coverage for latest current pipeline
#tmp=`curl -s https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/ | jq '.[0] | .id'`

latest=$(curl -s https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID} | jq '.coverage')

latest="${latest%\"}"
latest="${latest#\"}"
latest="${latest/.*}"
echo "pipline ${CI_PIPELINE_ID} coverage value = " "${latest}"

# get coverage for main
tmp=$(curl -s https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines\?ref\=main\&status\=success | jq '.[0] | .id')

# pass that into the curl below
main=$(curl -s https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${tmp} | jq '.coverage')
echo "main coverage = $main"
main="${main%\"}"
main="${main#\"}"
main="${main/.*}"
echo "main coverage value = $main"

# if latest >= main exit 0
if [ "$latest" -ge "$main" ]
then
  echo "Latest pipeline coverage >= main"
  exit 0
# else exit 1
else
  echo "Latest pipeline coverage < main"
  exit 1
fi
