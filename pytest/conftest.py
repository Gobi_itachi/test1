import os
def pytest_configure(config):
    ''' modifying the table pytest environment'''
    
    config._metadata['TEMP_VAR'] =  os.environ['TEMP_VAR']
