import pytest
import os
import logging

@pytest.mark.parametrize("input, expected" ,[
    ( [1, 2, 3, 4], 4),
    ( [7, 8, 9 , 10], 10)
])
def test_max(input, expected):
    "Verify the max function works"
    logging.debug("testing 1")
    Output = max(input)
    assert Output == expected
    logging.debug("testing 1 passed")



@pytest.mark.parametrize("input, expected" ,[
    ( [1, 2, 3, 4], 1),
    ( [7, 8, 9 , 10], 7)
])
def test_min(input, expected):
    "Verify the min function works"
    logging.debug("testing 2")
    Output = min(input)
    assert Output == expected
    logging.error("testing 2 passed")

def test():
    assert os.environ['TEMP_VAR'] == "hello he"

def test_new():
    assert os.environ['OLD_VAR'] == "dummy"

def inc(x):
    return x + 1


# def test_answer():
#     assert inc(4) == 5
